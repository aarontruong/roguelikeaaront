﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	
	public static GameController Instance;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	public Button hoboButton;
	public Button knightButton;
	public Button asianButton;
	public int charSelect;
	public int difficultySelect;
	public int attackPowers;
	public float moveTimes;


	public Text healthText;
	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private GameObject loadScreen;
	private GameObject splashScreen;
	private GameObject difficultySelection;
	private GameObject hobo;
	private GameObject knight;
	private Text levelText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	private int currentLevel = 1;



	void Awake () {
		if (Instance != null && Instance != this) 
		{
			DestroyImmediate(gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad(gameObject);
		boardController = GetComponent<BoardController> ();
		enemies = new List<Enemy>();
	}

	void Start()
	{
		Splash ();
	}

	private void InitializeGame()
	{
		if (currentLevel > 1) {
			difficultySelection = GameObject.Find ("DifficultySelection");
			splashScreen = GameObject.Find("SplashScreen");
			loadScreen = GameObject.Find ("Load Screen");
			difficultySelection.SetActive (false);
			splashScreen.SetActive(false);
			loadScreen.SetActive (false);
		}
		healthText = GameObject.Find ("Text").GetComponent<Text> ();
		healthText.text = "Health: " + playerCurrentHealth;
		settingUpGame = true;
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text> ();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive (true);
		enemies.Clear ();
		boardController.SetupLevel (currentLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}

	public void knightPress() 
	{
		attackPowers += 4;
		charSelect = 0;
		//knightButton.enabled = true;
		//Invoke ("EnableDifficulties", secondsUntilLevelStart);
		Invoke ("InitializeGame", secondsUntilLevelStart);
	}

	public void hoboPress() 
	{
		playerCurrentHealth += 50;
		charSelect = 1;
		//hoboButton.enabled = true;
		//knightButton.enabled = false;
		//Invoke ("EnableDifficulties", secondsUntilLevelStart);
		Invoke ("InitializeGame", secondsUntilLevelStart);

	}

	public void asianPress() 
	{
		charSelect = 2;
		Invoke ("InitializeGame", secondsUntilLevelStart);
	}

	public void easyPress()
	{
		difficultySelect = 0;
		EnableLoadScreen ();
	}

	public void mediumPress()
	{
		difficultySelect = 1;
		EnableLoadScreen ();
	}

	public void hardPress()
	{
		difficultySelect = 2;
		EnableLoadScreen ();
	}

	private void EnableDifficulties ()
	{
		difficultySelection = GameObject.Find ("DifficultySelection");
		difficultySelection.SetActive (true);
	}

	private void EnableLoadScreen() 
	{
		difficultySelection = GameObject.Find ("DifficultySelection");
		difficultySelection.SetActive (false);
		loadScreen = GameObject.Find ("Load Screen");
		loadScreen.SetActive (true);
		//hoboButton = hoboButton.GetComponent<Button>();
		//knightButton = knightButton.GetComponent<Button>();
	}

	private void Splash() 
	{
		//splashScreen = GameObject.Find ("SplashScreen");
		//splashScreen.SetActive (true);
		Invoke ("DisableSplashScreen", secondsUntilLevelStart);
	}

	private void DisableSplashScreen()
	{	
		splashScreen = GameObject.Find ("SplashScreen");
		splashScreen.SetActive (false);
		Invoke ("EnableDifficulties", secondsUntilLevelStart);
    }	

	private void DisableLoadScreen() 
	{
		loadScreen = GameObject.Find ("Load Screen");
		loadScreen.SetActive (false);
		settingUpGame = true;
		isPlayerTurn = true;
		areEnemiesMoving = false;
		Invoke ("InitializeGame", secondsUntilLevelStart);
	}

	/*private void EnableDifficultySelection()
	{
		difficultySelection = GameObject.Find ("DifficultySelection");

	}*/

	private void DisableLevelImage() 
	{
		levelImage.SetActive (false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}

	private void OnLevelWasLoaded(int levelLoaded)
	{
		currentLevel++;
		InitializeGame ();
	}

	void Update () {
		if (isPlayerTurn || areEnemiesMoving || settingUpGame) 
		{
			return;
		}

		StartCoroutine(MoveEnemies());
	}

	private IEnumerator MoveEnemies() 
	{
		areEnemiesMoving = true;

		yield return new WaitForSeconds(0.2f);

		foreach(Enemy enemy in enemies) 
		{
			enemy.MoveEnemy ();
			yield return new WaitForSeconds(enemy.moveTime);
		}


		areEnemiesMoving = false;
		isPlayerTurn = true;
	}

	public void AddEnemyToList(Enemy enemy)
	{
		enemies.Add (enemy);
	}

	public void AddKnightToList()
	{
		//ADD A KNIGHT TO THIS THING PUT STuFF IN PARAMETER
	}

	public void GameOver()
	{
		isPlayerTurn = false;
		SoundController.Instance.music.Stop ();
		SoundController.Instance.PlaySingle (gameOverSound);
		levelText.text = "You GOT REKT after " + currentLevel + " days...";
		levelImage.SetActive(true);
		enabled = false;
	}
}
